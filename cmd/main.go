// Package classification AuthService API.
// Consumes:
// - application/json
//
// Produces:
// - application/json
//
// Schemes: http
// Host: localhost
// BasePath: /
// Version: 0.0.1
//
// swagger:meta
package main

import (
	"github.com/Restream/reindexer"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"image-service/internal/config"
	"image-service/internal/repo"
	"image-service/internal/repo/repoFunctions"
	"image-service/internal/server"
	"image-service/internal/server/handlers"
	"image-service/internal/server/middleware"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	Version       = "v1.0.0"
	defaultRegion = "ru-msk"
)

func main() {

	logger := logrus.New()
	if logger == nil {
		logger.Fatal("New logger failed")
	}

	viper.SetEnvPrefix("aws")
	err := viper.BindEnv("endpoint")
	if err != nil {
		logger.Fatalf("error while bind env %v", err.Error())
	}

	sess, err := session.NewSession(&aws.Config{
		Endpoint: aws.String(viper.GetString("endpoint")),
		Region:   aws.String(defaultRegion),
	})

	// Init configuration and credentials
	configuration, err := config.NewConfiguration()
	if err != nil {
		logger.Fatal("config.NewConfiguration:", err.Error())
	}

	credentials, err := config.NewCredentials()
	if err != nil {
		logger.Fatal("config.NewCredentials:", err.Error())
	}

	var route = InitHandlers(InitHandlersDep{
		Conf:        configuration,
		Credentials: credentials,
		Logger:      logger,
		S3Session:   sess,
	})
	srv := server.NewServer(server.Options{
		ReadTimeout:       configuration.WebServer.Configuration.ReadTimeout,
		WriteTimeout:      configuration.WebServer.Configuration.WriteTimeout,
		ShutdownTimeout:   configuration.WebServer.Configuration.ShutdownTimeout,
		ReadHeaderTimeout: configuration.WebServer.Configuration.ReadHeaderTimeout,
		IdleTimeout:       configuration.WebServer.Configuration.IdleTimeout,
		MaxHeaderBytes:    configuration.WebServer.Configuration.MaxHeaderBytes,
	}, server.Dependencies{
		Handler: route,
		Logger:  logger,
		Config:  configuration,
	})
	go func() {
		err := srv.Start()
		if err != nil {
			logger.Fatalf("failed to Start server; err - %v", err)
		}
	}()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<-done
	logger.Error("Server Stopped")

	err = srv.Stop()
	if err != nil {
		logger.Error("failed to correct Stop server; err - %v", err)
	}
	time.Sleep(time.Second * 3)
	logger.Error("Server Exited Properly")
}

func InitRepoRegistry(DBConnect *reindexer.Reindexer) *repo.Registry {
	return &repo.Registry{
		AccountsRepo: repoFunctions.NewAccountsRepo(DBConnect),
	}
}

func InitHandlers(dep InitHandlersDep) http.Handler {
	requestIDMiddleware := middleware.NewRequestIDMiddleware()

	imageHandlers, err := handlers.NewImageHandlers(handlers.ImageHandlersDependencies{
		Logger:    dep.Logger,
		Conf:      dep.Conf,
		S3Session: dep.S3Session,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init imageHandlers")
	}

	middlewares := handlers.Middlewares{
		RequestIDMiddleware: requestIDMiddleware,
	}

	handlersApi := handlers.Handlers{
		Image: imageHandlers,
	}

	route := handlers.NewRoute(handlers.RouteDependencies{
		Logger:      dep.Logger,
		Middlewares: middlewares,
		Handlers:    handlersApi,
		Conf:        dep.Conf,
	}, handlers.RouteOptions{
		Version: Version,
		Cors:    handlers.RouteOptionsCors{},
	})

	return route
}

type InitHandlersDep struct {
	Conf        *config.Config
	Credentials *config.Credentials
	Logger      *logrus.Logger
	S3Session   *session.Session
}
