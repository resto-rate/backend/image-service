package convert

import (
	"image-service/internal/repo/repoModels"
	"image-service/internal/server/models"
)

func AccountRepo2Client(account *repoModels.Account) *models.AccountResponse {
	return &models.AccountResponse{
		AccountGUID: account.AccountGUID,
		Role:        account.Role,
		Login:       account.Login,
		Mobile:      account.Mobile,
		Name:        account.Name,
		Surname:     account.Surname,
	}
}
