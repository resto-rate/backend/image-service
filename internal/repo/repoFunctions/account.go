package repoFunctions

import (
	"github.com/Restream/reindexer"
	"image-service/internal/config"
	"image-service/internal/repo"
	"image-service/internal/repo/repoModels"
)

type AccountsRepo struct {
	db     *reindexer.Reindexer
	config *config.Config
}

func NewAccountsRepo(db *reindexer.Reindexer) *AccountsRepo {
	return &AccountsRepo{
		db: db,
	}
}

func (o *AccountsRepo) GetByAccountGUID(guid string) (*repoModels.Account, error) {
	elem, found := o.db.Query(repo.TableAccounts).WhereString("AccountGUID", reindexer.EQ, guid).
		OpenBracket().
		WhereInt64("Status", reindexer.EQ, repoModels.AccountsStatus.Active).
		Or().
		WhereInt64("Status", reindexer.EQ, repoModels.AccountsStatus.TempAccount).
		CloseBracket().
		Get()

	// Объект найден
	if found {
		model := elem.(*repoModels.Account)
		return model, nil
	}

	return nil, repo.ErrorNotFound
}

func (o *AccountsRepo) Upsert(account *repoModels.Account) error {
	return o.db.Upsert(repo.TableAccounts, account)
}
