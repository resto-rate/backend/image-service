package repoFunctions

import (
	"github.com/Restream/reindexer"
	"image-service/internal/repo"
	"image-service/internal/repo/repoModels"
)

type SessionsRepo struct {
	db *reindexer.Reindexer
}

func NewSessionRepo(db *reindexer.Reindexer) *SessionsRepo {
	return &SessionsRepo{
		db: db,
	}
}

func (o *SessionsRepo) Get(key string) (*repoModels.Session, error) {
	elem, found := o.db.Query(repo.TableSessions).
		WhereString("CookiesKey", reindexer.EQ, key).Get()

	if found {
		model := elem.(*repoModels.Session)
		return model, nil
	}

	return nil, repo.ErrorNotFound
}
