package models

// swagger:model GetOTPCodeReq
type GetOTPCodeReq struct {
	// login (phone number) to get otp code
	//
	// example: 7999999911
	Login string `json:"Login"`
}

// swagger:model GetOTPCodeRes
// result to correct request
type GetOTPCodeRes struct {
	// OtpGUID
	OtpGUID string `json:"OtpGUID"`
	// AuthExpiredSeconds
	AuthExpiredSeconds int64 `json:"AuthExpiredSeconds"`
}
