package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"image-service/internal/repo/repoModels"
	"image-service/internal/server/middleware"
	"net/http"
)

var (
	ErrSessionRefreshTokenExpired            = errors.New("refresh token of the session is expired")
	ErrRequestAndSessionLinkedObjAreNotEqual = errors.New("request and session linked objects are not equal")
	ErrManagerOrPartnerGUIDsAreEmpty         = errors.New("managerGUID or partnerGUID are empty")
	ErrLoginDataIsWrong                      = errors.New("user or password are empty")
	ErrOtpNotFound                           = NewErrUnitIsNotFound("OTP code")
	ErrNoAttemptsAnyMore                     = errors.New("no attempts any more")
	ErrOtpIsNotCorrect                       = errors.New("otp code is not correct")
	ErrLoginIsEmpty                          = errors.New("login is empty")
	ErrPhoneIsInBlackList                    = errors.New("phone number is in a black list")
	ErrTooManyPhones                         = errors.New("too many phones")
	ErrAccountIsBlocked                      = errors.New("account is blocked")
	ErrOtpOrLoginAreEmpty                    = errors.New("otp code or login are empty")
	ErrLinkedAccountIsDifferent              = errors.New("linked account is different")
	ErrCarsNotFound                          = NewErrUnitIsNotFound("cars")
	ErrTrailersNotFound                      = NewErrUnitIsNotFound("trailers")
	ErrDriversNotFound                       = NewErrUnitIsNotFound("drivers")
	ErrMissingReqId                          = errors.New("missing request id")
	ErrPartnerGUIDIsNotFound                 = NewErrUnitIsNotFound("PartnerGUID")
	ErrFeedbackGUIDIsNotFound                = NewErrUnitIsNotFound("FeedbackGUID")
	ErrFeedbackMessageIsEmpty                = errors.New("feedback message is empty")
	ErrTopicIsNotExist                       = errors.New("topic is not exist")
	ErrWholeWriteFiles                       = errors.New("could't write files")
	ErrSessionRefresh                        = errors.New("session couldn't do refresh")
	ErrDepRepoIsNil                          = NewErrDepIsNil("RepositoryRegistry")
	ErrDepLoggerIsNil                        = NewErrDepIsNil("Logger")
	ErrDepConfIsNil                          = NewErrDepIsNil("Config")
	ErrDepS3SessionIsNil                     = NewErrDepIsNil("S3Session")
	ErrDepCredentialsIsNil                   = NewErrDepIsNil("Credentials")
	ErrDepMtsMobileIsNil                     = NewErrDepIsNil("MtsMobile")
	ErrDepBlackListIsNil                     = NewErrDepIsNil("BlackList")
	ErrDepErrProcIsNil                       = NewErrDepIsNil("ErrProc")
	ErrDepCorpRouterIsNil                    = NewErrDepIsNil("CorpRouter")
	ErrDepApi1CIsNil                         = NewErrDepIsNil("Api1C")
	ErrDepProcessorIsNil                     = NewErrDepIsNil("Processor")
	ErrDepTrailerTypesRelatedIsNil           = NewErrDepIsNil("TrailerTypesRelated")
	ErrDepLoadingTypesRelatedIsNil           = NewErrDepIsNil("LoadingTypesRelated")

	ErrPreorderNumberIsNotCorrect   = errors.New("preorder number is not correct")
	ErrAuthIsRequired               = errors.New("authentication is required")
	ErrAccountNotFound              = errors.New("account not found")
	ErrRequiredRoleMissing          = errors.New("required role missing")
	ErrMissingArguments             = errors.New("missing arguments")
	ErrPartnerGUIDAndAccountAreDiff = errors.New("partnerGUID is not equal to account")
	ErrParseMultiPartForm           = errors.New("parse multipart form error")
	ErrCloseFile                    = errors.New("close file error")
	ErrBindEnv                      = errors.New("bind env error")
	ErrUploadFileToAws              = errors.New("failed to upload file to aws")
	ErrOrderGUIDIsMissed            = NewErrUnitIsMissed("OrderGUID")
	ErrPreorderGUIDIsMissed         = NewErrUnitIsMissed("PreorderGUID")
	ErrWrongOtpCode                 = errors.New("wrong otp code")
	ErrRequestAndSessionDataAreDiff = errors.New("request data isn't equal to sessions")
	ErrLimitActsToSign              = errors.New("limit acts to sign is reached")
	ErrWrongQtyActsBeSigned         = errors.New("wrong quantity of acts to be signed")
	ErrActsNotFound                 = NewErrUnitIsNotFound("acts")
	ErrArgumentsByActId             = NewErrUnitIsNotFound("arguments by actID")
	ErrTransportKeyIsMissed         = NewErrUnitIsMissed("TransportKey")
	ErrTransportKeyIsNotExist       = errors.New("transport key is not exist")
	ErrPasswordAuthDisabled         = errors.New("password auth disabled")
	ErrBankAccountIsEmpty           = errors.New("bankAccountGUID is empty")
	ErrCarsAreNotFound              = NewErrUnitIsNotFound("cars")
	ErrOrderPointsQtyLess           = errors.New("less than 2 OrderPoints")
	ErrMonthRange                   = errors.New("the param is not in the month range")
	ErrCityIsNotfound               = NewErrUnitIsNotFound("city")
	ErrUnloadingDate                = errors.New("invalid UnloadingDate")
	ErrFuelCardsAreNotFound         = NewErrUnitIsNotFound("fuel cards")
	ErrCreateOTPError               = errors.New("create otp failed")
	ErrFuelCardGUIDIsEmpty          = errors.New("fuel card GUID is empty")
	ErrFuelCardGUIDIsNotFound       = NewErrUnitIsNotFound("fuel card GUID")
	ErrFuelCardIsNotFound           = NewErrUnitIsNotFound("fuel card")
	ErrDuringFuelcardTransfer       = errors.New("the transfer can't be done, because of internal error")
	ErrAccountIsNotOwnerFuelCard    = errors.New("account is not owner of the fuel card")
	ErrFuelCardTransferTheSameCard  = errors.New("can't transfer to the same card")
	ErrPreorderIsNotFound           = NewErrUnitIsNotFound("preorder")
	ErrPartnerByLinkObject          = errors.New("can't find partner by linkObjectGUID")
	ErrEmployeeIsNotFound           = NewErrUnitIsNotFound("employee")
	ErrOfferIsNotFound              = NewErrUnitIsNotFound("offer")
	ErrOrderGUIDIsNotFound          = NewErrUnitIsNotFound("OrderGUID")
	ErrLimitOrderGUIDs              = errors.New("limit exceed")
	ErrLimitOrdersToSign            = errors.New("limit orders to sign is reached")
	ErrApi1cOrderIsNotFound         = NewErrUnitIsNotFound("order in api 1C")
	ErrFtpConnectionIsDisabled      = errors.New("ftp connection is disabled")
	ErrAccountIsNotOwnerOrder       = errors.New("account doesn't own the order")
	ErrOrdersAreNotFound            = NewErrUnitIsNotFound("orders")
	ErrFindStringRestriction        = errors.New("find string length restriction")
	ErrPretensionGUIDIsMissed       = NewErrUnitIsMissed("PretensionGUID")
	ErrPretensionsAreNotFound       = NewErrUnitIsNotFound("pretensions")
	ErrMtsSendMessage               = errors.New("send message mts failed")
)

type ErrDepIsNil struct {
	unit string
}

func GetAccountFromContext(ctx context.Context) (*repoModels.Account, bool) {
	contextKey := middleware.CtxKeyAccount
	u, ok := ctx.Value(contextKey).(*repoModels.Account)
	return u, ok
}

func GetSessionFromContext(ctx context.Context) (*repoModels.Session, bool) {
	contextKey := middleware.CtxKeySession
	s, ok := ctx.Value(contextKey).(*repoModels.Session)
	return s, ok
}

func (err ErrDepIsNil) Error() string {
	return fmt.Sprintf("%s is nil", err.unit)
}

func NewErrDepIsNil(unit string) error {
	return ErrDepIsNil{unit: unit}
}

type ErrUnitIsNotFound struct {
	unit string
}

func (err ErrUnitIsNotFound) Error() string {
	return fmt.Sprintf("%s: not found", err.unit)
}

func NewErrUnitIsNotFound(unit string) error {
	return ErrUnitIsNotFound{unit: unit}
}

type ErrUnitIsMissed struct {
	unit string
}

func (err ErrUnitIsMissed) Error() string {
	return fmt.Sprintf("%s is missed", err.unit)
}

func NewErrUnitIsMissed(unit string) error {
	return ErrUnitIsMissed{unit: unit}
}

func WriteError(w http.ResponseWriter, exterr error, statuscode int) error {
	var err error
	type result struct {
		Result bool   `json:"Result"`
		Error  string `json:"Error"`
	}

	// Формируем JSON
	jbody, err := json.Marshal(result{
		Result: false,
		Error:  fmt.Sprint(exterr),
	})
	if err != nil {
		return err
	}

	// Возвращаем код ошибки и текст ошибки
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statuscode)
	_, err = w.Write(jbody)

	return err
}

// TODO: Добавить sentry
func writeError(w http.ResponseWriter, logger logrus.FieldLogger, statuscode int, err error) {

	type errorStruct struct {
		ErrorID string `json:"ErrorID"`
		Message string `json:"Message"`
	}

	var responseErr errorStruct

	if err != nil {
		responseErr.Message = err.Error()
	}

	responce, err := json.Marshal(responseErr)
	if err != nil {
		logger.Errorf("writeError - error: %w", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statuscode)
	_, err = w.Write(responce)
	if err != nil {
		logger.Errorf("writeError - error: %w", err)
		return
	}
}

func writeJson(w http.ResponseWriter, logger logrus.FieldLogger, body interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	jsonBody, err := json.Marshal(body)
	if err != nil {
		logger.WithError(err).Error("json.Marshal in response - error")
	}

	_, err = w.Write(jsonBody)
	if err != nil {
		logger.WithError(err).Error("w.Write in response - error")
	}
}

func writeBody(w http.ResponseWriter, logger logrus.FieldLogger, body []byte) {
	_, err := w.Write(body)
	if err != nil {
		logger.WithError(err).Error("w.Write in response - error")
	}
}
