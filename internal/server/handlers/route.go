package handlers

import (
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"image-service/internal/config"
	"net/http"
)

type ImageI interface {
	Upload(w http.ResponseWriter, r *http.Request)
	Health(w http.ResponseWriter, r *http.Request)
}

type MiddlewareI interface {
	Middleware(next http.Handler) http.Handler
}

type ConfI interface {
	Get(w http.ResponseWriter, r *http.Request)
}

type Middlewares struct {
	RequestIDMiddleware MiddlewareI
	AuthMiddleware      MiddlewareI
}

type Handlers struct {
	Image ImageI
}

type RouteDependencies struct {
	Logger      logrus.FieldLogger
	Middlewares Middlewares
	Handlers    Handlers

	Conf *config.Config
}

type RouteOptions struct {
	Version string
	Cors    RouteOptionsCors
}

type RouteOptionsCors struct {
	AllowedOrigins   []string
	AllowedHeaders   []string
	AllowCredentials bool
	Debug            bool
}

func NewRoute(dep RouteDependencies, opt RouteOptions) http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/health", dep.Handlers.Image.Health)

	image := r.PathPrefix("/image").Subrouter()
	image.Use(dep.Middlewares.RequestIDMiddleware.Middleware)
	//image.Use(dep.Middlewares.AuthMiddleware.Middleware)

	// swagger:route POST /image/upload uploadImage uploadImage
	//
	// Upload image to s3
	//
	//     Parameters: []
	//
	//     Responses:
	//       200:
	//		 	description: status ok
	//			schema:
	//				$ref: "#/model/AuthMethodsResponse"
	//       500:
	image.HandleFunc("/upload", dep.Handlers.Image.Upload).Methods(http.MethodPost)

	// swagger:route POST /auth/otp getOtpCode getOtpCode
	//
	//	Send to user otp code to phone by sms (or just to terminal)
	//
	//  parameters:
	//		+ name: Login
	//      in: body
	//		type: GetOTPCodeReq
	//      description: user login (phone number)
	//      required: true
	//      allowEmpty:  false
	//
	//	responses:
	//  	200:body:GetOTPCodeRes
	//      400:
	//      401:
	//auth.HandleFunc("/otp", dep.Auth.GetOTPCode).Methods(http.MethodPost)

	corsHandler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Origin", "Authorization", "Content-Type"},
	})

	return corsHandler.Handler(r)
}
