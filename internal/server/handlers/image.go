package handlers

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"image-service/internal/config"
	"image-service/internal/errproc"
	"mime/multipart"
	"net/http"
	"time"
)

type ImageHandlers struct {
	logger    logrus.FieldLogger
	conf      *config.Config
	errProc   *errproc.ErrProc
	s3Session *session.Session
}

type ImageHandlersDependencies struct {
	Logger    logrus.FieldLogger
	Conf      *config.Config
	ErrProc   *errproc.ErrProc
	S3Session *session.Session
}

func NewImageHandlers(dep ImageHandlersDependencies) (*ImageHandlers, error) {
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil

	}
	if dep.S3Session == nil {
		return nil, ErrDepS3SessionIsNil

	}

	logger := dep.Logger.WithField("", "AuthHandlersCI")

	return &ImageHandlers{
		logger:    logger,
		conf:      dep.Conf,
		s3Session: dep.S3Session,
	}, nil
}

func (o *ImageHandlers) Upload(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Upload")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	uploader := s3manager.NewUploader(o.s3Session)

	file, header, err := r.FormFile("imageFile")
	if err != nil {
		fmt.Printf("failed to open file, %v", err)
	}
	defer func(file multipart.File) {
		err := file.Close()
		if err != nil {
			exception := errproc.NewException(ErrCloseFile).WrapCloseFileError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusUnauthorized, ErrCloseFile)
			return
		}
	}(file)

	viper.SetEnvPrefix("aws")
	err = viper.BindEnv("bucket")
	if err != nil {
		exception := errproc.NewException(ErrBindEnv).WrapBindEnvError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrBindEnv)
		return
	}
	fmt.Println(header.Filename)

	hasher := sha256.New()
	hasher.Write([]byte(fmt.Sprintf("%s%d", header.Filename, time.Now().Unix())))
	filename := hex.EncodeToString(hasher.Sum(nil))

	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket:               aws.String(viper.GetString("bucket")),
		Key:                  aws.String(filename),
		ACL:                  aws.String("public-read"),
		ServerSideEncryption: aws.String("AES256"),
		ContentDisposition:   aws.String("attachment"),
		Body:                 file,
	})
	if err != nil {
		exception := errproc.NewException(ErrUploadFileToAws).WrapUploadFileAwsError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrUploadFileToAws)
		return
	}

	//account.ImageURL = result.Location

	/*err = o.repositoryRegistry.AccountsRepo.Upsert(account)
	if err != nil {
		exception := errproc.NewException(ErrPartnerGUIDAndAccountAreDiff).WrapAccountIsNotOwner().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusUnauthorized, ErrPartnerGUIDAndAccountAreDiff)
		return
	}

	response := convert.AccountRepo2Client(account)*/
	//writeJson(w, logger, response)
	writeJson(w, logger, result.Location)
}

func (o *ImageHandlers) Health(w http.ResponseWriter, r *http.Request) {
	writeJson(w, nil, "Hello")
}
