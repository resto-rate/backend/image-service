package server

const (
	RoleAdmin = "ADMIN"
	RoleUser  = "USER"
)

var UserLinkObjectType map[string]string
var RoleNames map[string]string

func init() {
	// Связь роли и типа связанного объекта
	UserLinkObjectType = make(map[string]string)
	//UserLinkObjectType[RoleUser] = repoModels.TableUsers
	//UserLinkObjectType[RoleAdmin] = repoModels.TableAdmins
}
