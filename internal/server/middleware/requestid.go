package middleware

import (
	"context"
	"github.com/getsentry/sentry-go"
	"github.com/google/uuid"
	"net/http"
)

type RequestIDMiddleware struct{}

type ctxKeyRequestID string

var CtxKeyRequestID ctxKeyRequestID

func init() {
	CtxKeyRequestID = "XRequestID"
}

func (mw *RequestIDMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		requestID := r.Header.Get("xRequestID")
		if requestID == "" {
			requestID = "G-" + uuid.New().String()
		}

		scope := sentry.NewScope()
		scope.SetTag("RequestID", requestID)
		hub := sentry.NewHub(sentry.CurrentHub().Client(), scope)
		ctxSentry := sentry.SetHubOnContext(r.Context(), hub)

		r = r.WithContext(ctxSentry)
		r = r.WithContext(context.WithValue(r.Context(), CtxKeyRequestID, &requestID))

		next.ServeHTTP(w, r)
	})
}

func NewRequestIDMiddleware() *RequestIDMiddleware {
	return &RequestIDMiddleware{}
}
