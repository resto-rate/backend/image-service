package middleware

import (
	"fmt"
	"net/http"
)

type ctxKeySession string
type ctxKeyAccount string

var CtxKeySession ctxKeySession
var CtxKeyAccount ctxKeyAccount

func init() {
	CtxKeySession = "session"
	CtxKeyAccount = "account"
}

// Жесткое закрытие соединения, до завершения приёма всего запроса
func hardClose(w http.ResponseWriter) error {
	hj, ok := w.(http.Hijacker)
	if !ok {
		return fmt.Errorf("hijacker error")
	}

	conn, _, err := hj.Hijack()
	if err != nil {
		return fmt.Errorf("hijacker get conn error")
	}

	err = conn.Close()
	if err != nil {
		return fmt.Errorf("hijacker conn close error")
	}

	return nil
}
