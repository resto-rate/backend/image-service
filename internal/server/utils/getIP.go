package utils

import (
	"net"
	"net/http"
)

func GetClientIP(r *http.Request) (string, error) {
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return "", err
	}

	// Преобразование адреса к общему виду
	userIP := net.ParseIP(ip)
	if userIP == nil {
		return "", err
	}

	return userIP.String(), nil
}
